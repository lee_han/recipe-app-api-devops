variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api"
}

variable "contact" {
  default = "email@email.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "778052703984.dkr.ecr.ap-southeast-2.amazonaws.com/recipe-app-code:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for Proxy"
  default     = "778052703984.dkr.ecr.ap-southeast-2.amazonaws.com/recipe-app:latest"
}

variable "django_secret_key" {
  description = "Secret key for recipe app"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "elcyen.com"
}

variable "subdomain" {
  description = "Subdomain per environmant"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}